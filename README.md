# Project Brief:

1. The requirements is to Provision a simple web application that has backend and Database Store
   The app should be a simple app or an off the shelf docker image.

2. Create an orchestrated cluster of nodes using any provisioning tool of your choice.

3. Deploy the application using an orchestration Layer to scale the application


# Option 1:

A simpler option for a web application will use the following cloud services/Resources for the project (not limited to this list):

0. Keypair
1. EC2 t2.micro = webserver hosting a simple python application that retrieves data from a mySQL DB
2. Db.t2.micro or db.m1.small = MySQL
3. Infrastructure(CF template) = CloudFormation as deployment tool
4. CI/CD = AWSCodePipeline
5. S3 Bucket = Repository N/B: make sure versioning is enabled on the S3 bucket, S3 config not included in CF template
6. Snapshot/Backup = Lambda (I have not included this functionality with this project)
7. Application LoadBalancer

# Prerequisites:

Docker, pip, python have should be pre-installed on the system used to create this initial image.


# Simple Python Application:
-----------------------------

# create a folder in any directory of your choice. (note this dir as "script")

In the script folder create a simple file called DockerFile, to extend python:2 image and install mySQL-python package.

In the Dockerfile add:

=========================================



    FROM python:2
    WORKDIR /usr/src/app
    RUN pip install MySQL-python
    COPY . .
    CMD ["python", "./script.py"]
  
  
  
=========================================


 Copy the contents of this directory in 1 above to the Working dir in the dockerfile "WORKDIR" 

 execute a script.py file

 Next add the following config lines scripts.py file 


===========================================================




     #!/usr/bin/python

     import MySQLdb

     db = MySQLdb.connect("mysql-server","root","root2481","pgdb")
     cursor = db.cursor()
     cursor.execute("SELECT * FROM person")
     data = cursor.fetchone()
     fname = data[0]
     lname = data[1]
     print "fname=%s, lname=%s % (fname, lname)
     db.close()
     
     
     
============================================================

 Go back in your directory from 1 above and execute docker build command
   docker build -t my-script .
    
 test that the script when launched from one container connects with the mysql db in a different container
   docker run -it --rm --network my-network my-script.


 This Image will be deployed into your EC2 instance using CloudFormation.

# VPC Stack - Brief:
--------------------

VPC, IGW, routing table with one public Route, two public subnets in 2 availability zones with 2 public routes associations for high availability and failover. Two Private Subnets for DB multizone requirements, 1 Autoscaling 

group for the webserver Launch configuration. One Frontend Security group which allows http/s requests and SSH into the 

EC2 instances and another security group which ONLY allows requests ONLY from the webserver EC2 instances via port 3306 to the DB instance and 

an SSH for Database Patching.

Also we need an App Loadbalancer (to load balance the webservers in the public subnets) and an AutoScaling group (with specified min and max sizes 

for the web instances). Additional Configuration for processor usage and cloudwatch could be used to manage autoscaling but this will not be included in this simple project.
Within the launch configuration we specify the exact Application AMI and any other services we wish to run via cf-init with the necessary Metadata. 

# Build/Deployment:

The New CD pipeline for creating the stack 

S3===>CodePipeLine===>CloudFormation===>New_Stack


# Change sets pipeline:

ChangeSet_Template===>S3===>CodePipeLine===>changeSet

ChangeSet===>CodePipeline===>CloudFormation===>Amended_Stack

# Validate the Template

aws cloudformation validate-template --template-body file://Sensyne_VPC_App.js

# Launch the Template

aws cloudformation create-stack --stack-name MultiRegionMultiAZAccountVPC --template-body file://Sensyne_VPC_App.js.json



# Option 2:

Lets make it more fun, lets Use AMAZON Elastic Container Service (ECS) to orchestrate, deploy and potentially scale  our containerized web-based microservice.

Assumption 1: The web application 
has been built into a fully baked AMI that can be run on a Docker Container. This ECS stack covers the Front End infrastructure only, additional stack is required to build the backend/database infrastructure.

# For this option the tools to use are:

0. Keypair
1. EC2 t2.micro = webserver
2. ECS Autoscaling Group
3. Infrastructure(CF template)=CloudFormation as deployment tool
4. CI/CD = AWSCodePipeline
5. S3 Bucket = Repository N/B: make sure versioning is enabled on the S3 bucket
6. Elastic Container Service (Service Definition, Task Definition, Cluster definition, Application Load Balancer, Target group)
7. CloudWatch for Alarms
8. IAM service Role


# Validate the Template2

aws cloudformation validate-template --template-body file://sensyne_test_app_on_ecs.js

# Launch the Template2

aws cloudformation create-stack --stack-name SensyneECSProject --template-body file://sensyne_test_app_on_ecs.js